#######################################################################################################
#    Description of the data and codes for Oziel et al. 2020 published in Nature Communication        #
# "Faster Atlantic currents drive poleward expansion of temperate marine species in the Arctic Ocean" #
#                                           by Laurent Oziel.                                         #
#######################################################################################################

CODES DESCRIPTION:

1) Figure 1 is a map of the studied area.

2) Figure 2 shows the EOF analysis of the Sea Level fields.

3) Figure 3 compares absolute geostrophic velocities at the beginning/end [...]
            of the timeseries and associated trend.

4) Figure 4 compares EXP.1 results for 1998 and 2015 (extremums)

5) Figure 5. Comparet trends from EXP.1 (reference = varying temperature and currents), [...] 
                                  EXP.2 (role of currents), [...]
                                  EXP.3 (role of temperature), [...] 
                                  and Ocean color ("true" observation)


ANCILARIES:

The scripts used to plot Figures require some Matlab (c) toolboxes.
Once downloaded, they need to be set in the working path.
To add a folder in the working path, you have to run:

addpath(genpath('/path/'))

Here the list of freely available toolboxes you need:

i)   The m_map package: https://www.eoas.ubc.ca/~rich/map.html
ii)  The geostrophic velocity calculation Robbert B. Scott's scripts ssh2vel.m:
        http://stockage.univ-brest.fr/~scott/MatLab/
iii) Optional, a package to better export figure:
        https://fr.mathworks.com/matlabcentral/fileexchange/23629-export_fig
iv)  A package to improve subplots: 
        https://fr.mathworks.com/matlabcentral/fileexchange/27991-tight_subplot-nh-nw-gap-marg_h-marg_w
v)   A package for ocean colormaps: 
        https://fr.mathworks.com/matlabcentral/fileexchange/57773-cmocean-perceptually-uniform-colormaps



DATA DESCRIPTION:


1) EXP1

It contains 19 files. One file for each year between 1998 and 2015. Every file is structured like this:
name file: data%d_tinf4_an10.mat %d:year considered
tinf: temp. used as threshold for the patch
lonadv&latadv: Matrix with 2 rows. The first contains the starting position of the patch advected, while the second the final position.
tI2:temperature field in March
tF2:temperature field in August
xvc&yvc: Arrays with longitude & latitude of the temperature (for the plot)
pic:PIC in July-August-September
lonpic latpic: longitude & latitude of the PIC field (for the plot)
dayi: date of the beginning of the experiment
dayf: date of the end of the experiment
delta0: grid definition for the virtual particles (i.e. spacing between particles)
NOTE: the time-step is 3h, so you may you can re-construct the date vector by doing:
date_vec=[datenum(dayi):1/8:datenum(dayf)];

2) EXP2


It contains 1 file: data_1998_2016_tinf4tsup20deg_over50N7W_an12.mat
I.e. advection of the same patch in order to see the trend in the displacement due to currents over the years.
The file contains the following variables:
ny: number of year over which the climatology is done (19 if we consider 1998-2016 period)
tI2: Climatology of temperature in March.
xvc & yvc: their longitude and latitude
lons,lats: coordinates of the starting patch
lonlim,latlim: longitudinal and latitudinal limits for the starting patch
tinf,tsup: inferior and superior limit for the temperature (tsup=20 degree has been put just to make the script work)
dist_tot: matrix number of columns=length(lons) and ny rows. Thus for each row, I have the total distance travelled by each particle.
mask: it has the same size of dist_tot. For each point, if the values is 1, it means that the final longitude of the particle was greater than 32°E (32°E is lonbloom)


3) EXP3

It contains 19 files. One file for each year between 1998 and 2015. Every file is structured like this:
name file: data%d_tinf4_an14.mat %d:year considered
tinf: temp. used as threshold for the patch
lonadv&latadv: Matrix with 2 rows. The first contains the starting position of the patch advected, while the second the final position.
tI2:temperature field in March
tF2:temperature field in August
xvc&yvc: Arrays with longitude & latitude of the temperature (for the plot)
pic:PIC in July-August-September
lonpic latpic: longitude & latitude of the PIC field (for the plot)

4) GVEL

Monthly Geostrophic velocities maps for each year (lat, lon, month). They contain Absolute Gesotrophic Velocities for both components U (toward east) and V toward North.
The unit is m/s.  The full signal is provided (UG, VG) together with the velocities without the seasonal cycle (UG_WSC,VG_WSC).
The CMEMS product was using the MDT produced by CLS version 13 (MDT_CLS13). The sea ice concentrations greater than 15% are masked.

5) MASKS

MASKS used for selecting only virtual particules located at a certain distance from the coast (in our case 180 km).
Files are named: MASK_DISTANCEFROMCOAST_EXP#_%d.mat, %d = year, # = experience number (1, 2 or 3).
variable: dists_min show the minimum distance from the coast for each particles in km.

MASK_FOR_EHUX_BLOOM_v4.mat is the mask used to delineate the area (eastern Barents Sea) where the leading edge is derived.
It contains lat, lon and MASK. When MASK = 0 the area must exclude particles; when MASK = 1 the particles are taken into account.


6) Remaining binaries description:

LEANDINGEDGE.mat : leading edge adapted from Neukermans et al. 2018.
MDT.mat : MDT version CLS_13 imported in matlab format, please find the original on aviso:
https://www.aviso.altimetry.fr/en/data/products/auxiliary-products.html

PIC.mat : yearly (maximum of the July-August-September period) valued of PIC (Particulate inorganic Carbon, mol/m3);
Find orginal files on globcolor: http://hermes.acri.fr/

TIME.mat : time vector in matlab format. type datestr(time) for string format.

TRENDS : trend (least-squares linear regression) maps of non-seasonal geostrophic velocities with p-values from a two-sided t-test.

DISTANCES_all.mat : leading-edge positions time-series from Lagrangian simulations (EXP1, 2 and 3) and from PIC Ocean Color.

LATLON.mat : Longitude and Latitude vectors of the studied area.

LE_EXPANSION_95th_175km.mat : 
Statistics about the linear regression (trend) calculation of the leading edge expansion using the 95th percentile 
and a distance of 175km from the coast. p_exp# are the coeficient of the linear regression.
for example, the linear regression (trend) of the increase distance of the leading-edge from the reference point (33degE, 70degN)
can be expressed as: p_exp1(1) * date_vec +p_exp2(2); 
x_exp# are the longitudes of the leading edge position.
y_exp# are the latitudes of the leading edge position.
dr_exp# are the 95% confidence interval.

COLORMAP_BLUE_BS.mat : custom colormap for Figure 1