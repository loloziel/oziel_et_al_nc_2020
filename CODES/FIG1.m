clear all
close all
clc


hFig=figure;
set(gcf,'PaperPositionMode','auto')
set(hFig, 'Position', [0 0 1400 800])

[ha, pos] = tight_subplot(1, 1);

m_proj('Equidistant cylindrical','lon',[-20 70],'lat',[60 80]);
hold on
[ELEV,LON_ELEV,LAT_ELEV]=m_etopo2([-20 70 60 80]);
ELEV(ELEV<-4000)=-4000;
[CS,CH]=m_contourf(LON_ELEV,LAT_ELEV,ELEV,[-4000:1000:-1000 -500 -300 -200 -100 0],'edgecolor','none');
m_grid('linewi',2,'tickdir','out','fontsize',20,'XTick',[-20:20:70]);
m_gshhs_i('patch',[.6 .6 .6],'edgecolor','none');


load ../DATA/COLORMAP_BLUE_BS.mat
colormap(cmap);
caxis([-4000 000]);

[ax,h]=m_contfbar([.55 .92],.1,CS,CH,'endpiece','no','axfrac',.03);
title(ax,'meters');
set(ax,'fontsize',18,'fontweight','bold')


ax3=axes('position',[.75 .7 .3 .3]);
m_proj('ortho','lat',60','long',0');
m_coast('patch',[.6 .6 .6],'edgecolor','none');
m_grid('linest','-','Xticks',[],'YTicks',[]);

lat_rect=[60*ones(1,91) 80*ones(1,91) 60]; % Outline of a box
lon_rect=[-20:70 70:-1:-20 -20];
m_line(lon_rect,lat_rect,'color','k','linewi',3);
 
%export_fig ../FIGURES/FIG1.png -append -nocrop -png -r300 -transparent
%export_fig ../FIGURES/FIG1_low.png -append -nocrop -png -r150 -transparent
%export_fig ../FIGURES/FIG1.pdf -append -nocrop -eps -r300 -transparent -painters