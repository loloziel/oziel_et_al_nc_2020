clear all
close all
clc

% check min/max of the EOF time-series %
load ../DATA/EOF.mat
pc12=pc(1,:)+pc(2,:);

[MIN,i_min]=min(pc12);
[MAX,i_max]=max(pc12);
keep i_min i_max

load ../DATA/TIME.mat
disp(['date min = ',datestr(time(i_min))]);
disp(['date max = ',datestr(time(i_max))]);
clear all

load ../DATA/LATLON.mat

hFig=figure;
set(gcf,'PaperPositionMode','auto');
set(hFig, 'Position', [0 0 800 1200]);
gap=4; % This allows to plot one every four arrows in order not to overcrowd the plot

load ../DATA/GVEL_1993.mat UG_WSC VG_WSC
% select the minimum month %
ug_1993=squeeze(nanmean(UG_WSC(:,:,12),3));
vg_1993=squeeze(nanmean(VG_WSC(:,:,12),3));
% derive current speed from velocities %
gvel_1993=double(sqrt(ug_1993.^2+vg_1993.^2)); % we select the month of December, minimum of the pc1+pc2

load ../DATA/GVEL_2015.mat UG_WSC VG_WSC
% select the Maximum month %
ug_2015=squeeze(nanmean(UG_WSC(:,:,12),3));
vg_2015=squeeze(nanmean(VG_WSC(:,:,12),3));
% derive current speed from velocities %
gvel_2015=double(sqrt(ug_2015.^2+vg_2015.^2)); % we select the month of December, maximum of the pc1+pc2

[ha, pos] = tight_subplot(3, 1);
cmap=cptcmap('seminf-haxby','ncol',20);%cmocean('speed');

axes(ha(1));
m_proj('Equidistant cylindrical','lon',[-20 70],'lat',[60 80]);
[xxi,yyi]=meshgrid(LON,LAT);
m_contourf(xxi,yyi,gvel_1993,[0:0.02:0.1],'edgecolor','none');hold on;
%cptcmap('seminf-haxby','ncol',20);
cmap=cmocean('amp',12);
cmap(11:12,:)=[];%get rid of dark red
colormap(gca,cmap);
m_quiver(xxi(1:gap:end,1:gap:end),yyi(1:gap:end,1:gap:end),ug_1993(1:gap:end,1:gap:end),vg_1993(1:gap:end,1:gap:end),1.5,'color','k','linewidth',1.5);
m_grid('linewi',2,'tickdir','out','fontsize',20,'XTick',[-20:20:100],'XTickLabel',[],'backcolor',[.3 .3 .3]);
m_gshhs_i('patch',[.6 .6 .6],'edgecolor','none');
m_text(-18,78,'a.','fontsize',30,'fontweight','bold','backgroundcolor',[1 1 1]);
m_text(50,62,'1993','fontsize',30);

axes(ha(2));
m_proj('Equidistant cylindrical','lon',[-20 70],'lat',[60 80]);
m_contourf(xxi,yyi,gvel_2015,[0:0.02:0.1],'edgecolor','none');hold on;
m_quiver(xxi(1:gap:end,1:gap:end),yyi(1:gap:end,1:gap:end),ug_2015(1:gap:end,1:gap:end),vg_2015(1:gap:end,1:gap:end),1.5,'color','k','linewidth',1.5);
%cptcmap('seminf-haxby','ncol',20);
%cmocean('amp',11);
colormap(gca,cmap);
m_grid('linewi',2,'tickdir','out','fontsize',20,'XTickLabel',[],'XTick',[-20:20:100],'backcolor',[.3 .3 .3]);
m_gshhs_i('patch',[.6 .6 .6],'edgecolor','none');
m_text(-18,78,'b.','fontsize',30,'fontweight','bold','backgroundcolor',[1 1 1]);
m_text(50,62,'2015','fontsize',30);

hc = colorbar;
set(hc,'location','eastoutside','position',[0.78 0.36 0.02 0.59],'fontsize',20,'Ticks',[0:.02:0.1],'TickLabels',[0:2:10]);
%set(hc,'colormap',cmap);
xlabel(hc,'Surface Geostrophic Current Velocities (cm s^-^1)','fontsize',20);


load ../DATA/TRENDS.mat


axes(ha(3))
m_proj('Equidistant cylindrical','lon',[-20 70],'lat',[60 80]);
m_pcolor(LON,LAT,GEOS_VEL_TREND); % the trend need to be converted from m/s/day to mm/s/year
shading flat;caxis([-2 2]);
caxis([-2.2 2.2]);
m_gshhs_i('patch',[.6 .6 .6],'edgecolor','none');
cmocean('balance',11);
m_grid('linewi',2,'tickdir','out','fontsize',20,'XTick',[-20:20:100],'backcolor',[.3 .3 .3]);
m_text(-18,78,'c.','fontsize',30,'fontweight','bold','backgroundcolor',[1 1 1]);
m_text(45,62,'TREND','fontsize',30);


% average relative increase in boxes % 
% can be adapted to the box you want %

ind_lon1=find(LON>=5 & LON<=7);
ind_lat1=find(LAT>=72 & LAT <= 72.5);
x=round(nanmean(nanmean(PERCENT(ind_lat1,ind_lon1))));
m_text(0,78.5,['+ ',num2str(x),' %'],'fontsize',20,'fontweight','bold','color','k','backgroundcolor','w');
m_line([6.25 6.25],[72.5 77.5],'color','k','linewidth',2);
m_line([5 7 7 5 5],[72 72 72.5 72.5 72],'color','k','linewidth',2);

ind_lon2=find(LON>=16.5 & LON<=17.5);
ind_lat2=find(LAT>=70 & LAT <= 71);
x=round(nanmean(nanmean(PERCENT(ind_lat2,ind_lon2))));
m_text(15,67,['+ ',num2str(x),' %'],'fontsize',20,'fontweight','bold','color','k','backgroundcolor','w');
m_line([16.5 17.5 17.5 16.5 16.5],[70 70 71 71 70],'color','k','linewidth',2);
m_line([17 17],[68 70],'color','k','linewidth',2);

ind_lon3=find(LON>=30 & LON<=38);
ind_lat3=find(LAT>=71 & LAT <= 72);
x=round(nanmean(nanmean(PERCENT(ind_lat3,ind_lon3))));
m_text(26,78.5,['+ ',num2str(x),' %'],'fontsize',20,'color','k','fontweight','bold','backgroundcolor','w');
m_line([30 38 38 30 30],[71 71 72 72 71],'color','k','linewidth',2);
m_line([34 34],[72 77.5],'color','k','linewidth',2);

ind_lon5=find(LON>=43 & LON<=46);
ind_lat5=find(LAT>=72 & LAT <= 76);
nanmean(nanmean(PERCENT(ind_lat5,ind_lon5)));
m_text(43,78.5,['+ ',num2str(x),' %'],'fontsize',20,'color','k','fontweight','bold','backgroundcolor','w');
m_line([43 46 46 43 43],[72 72 76 76 72],'color','k','linewidth',2);
m_line([44.5 44.5],[76 77.5],'color','k','linewidth',2);

ind_lon6=find(LON>=10.5 & LON<=12.5);
ind_lat6=find(LAT>=65 & LAT <= 67);
x=round(nanmean(nanmean(PERCENT(ind_lat6,ind_lon6))));
m_text(10,61.5,['+ ',num2str(x),' %'],'fontsize',20,'fontweight','bold','color','k','backgroundcolor','w');
m_line([11.5 11.5],[62.5 65],'color','k','linewidth',2);
m_line([10.5 12.5 12.5 10.5 10.5],[65 65 67 67 65],'color','k','linewidth',2);


% draw colorbar %
hc2 = colorbar;
set(hc2,'location','eastoutside','position',[0.78 0.05 0.02 0.28],'fontsize',20,'Ticks',[-2:.4:2],'TickLabels',[-2:.4:2]);
xlabel(hc2,'Absolute trend  (mm s^-^1 year^-^1)','fontsize',20)

% uncomment for printing figure %
%export_fig FIG3.png -append -nocrop -png -r300 -transparent
%export_fig FIG3_low.png -append -nocrop -png -r150 -transparent
%export_fig FIG3.pdf -append -nocrop -pdf -r300 -transparent -painters
%close