clear all
close all
clc


% choose color for initial virtual particles %


color1=[205,133,63]/256; % light brown/orange
%color2=[91, 60, 17]/256; % brown / orange
%color3=[139, 60, 139]/256; % magenta/purple



% FIGURE 4 %

hFig=figure;
set(gcf,'PaperPositionMode','auto')
set(hFig, 'Position', [0 0 1800 700])

[ha, pos] = tight_subplot(1, 2,[.03 .03],[.1 .05],[.05 .1]);

axes(ha(1));

% monthly field of PIC available on Globcolor website %
% Here we only selected summer months (July-August-September) %

load /Users/laurentoziel/Documents/INFLOW_SHELVES/GIN_SEAS/DATA/PIC.mat
pic=squeeze(pic(:,:,1)); % select summer 1998

% configure colormap %
cmap=cmocean('ice',12);
cmap=flipud(cmap);
cmap(1,:)=1;
cmap(11:12,:)=[];
colormap(cmap);
%%%%%%%%%%%%%%%%%%%%%%%


m_proj('Equidistant cylindrical','lon',[-20 70],'lat',[60 80]);hold on
m_contourf(lon,lat,pic,[0:.001:.1],'linestyle','none');

m_grid('linewi',2,'tickdir','out','fontsize',20,'XTick',[-20:20:100],'backgroundcolor',[.3 .3 .3]);
caxis([0 0.01]);
m_gshhs_i('patch',[.6 .6 .6],'edgecolor','none');

load ../DATA/EXP1/data1998_tinf4_an10.mat
load ../DATA/MASK_DISTANCEFROMCOAST_EXP1_1998.mat
ind = find(dists_min<=180);
m_plot(lonadv(1,ind),latadv(1,ind),'s','color',color1,'markersize',3,'MarkerFaceColor',color1)
m_plot(lonadv(2,ind),latadv(2,ind),'or','markersize',2,'MarkerFaceColor','r')


m_contour(xvc,yvc,tI2,[4 4],'-','linewidth',3,'color',color1);
m_contour(xvc,yvc,tF2,[4 4],'-r','linewidth',3);

m_text(-2,66,'Winter 4°C','color',color1,'fontsize',20,'rotation',90,'backgroundcolor',[.9 .9 .9]);
m_text(-12,71,'Summer 4°C','color','r','fontsize',20,'rotation',90,'backgroundcolor',[.9 .9 .9]);


m_text(-18,78,'a. ','fontsize',30,'backgroundcolor',[1 1 1]);
title('1998','fontsize',30);
pic_1998=pic;
lon_pic_1998=lon;
lat_pic_1998=lat;


axes(ha(2));

load ../DATA/PIC.mat
pic=squeeze(pic(:,:,18)); %select summer 2015

cmap=cmocean('ice',12);
cmap=flipud(cmap);
cmap(1,:)=1;
cmap(11:12,:)=[];
colormap(cmap);

m_proj('Equidistant cylindrical','lon',[-20 70],'lat',[60 80]);hold on
m_contourf(lon,lat,pic,[0:.001:.1],'linestyle','none');

caxis([0 0.01]);
m_grid('linewi',2,'tickdir','out','fontsize',20,'YTickLabel',[],'XTick',[-20:20:100],'backgroundcolor',[.3 .3 .3]);
m_gshhs_i('patch',[.6 .6 .6],'edgecolor','none');

load ../DATA/EXP1/data2015_tinf4_an10.mat
load ../DATA/MASK_DISTANCEFROMCOAST_EXP1_2015.mat
ind = find(dists_min<=180); % select particles closer than 180 km from the coast
h3=m_plot(lonadv(1,ind),latadv(1,ind),'s','color',color1,'markersize',3,'MarkerFaceColor',color1)
h4=m_plot(lonadv(2,ind),latadv(2,ind),'or','markersize',2,'MarkerFaceColor','r')


m_contour(xvc,yvc,tI2,[4 4],'-','color',color1,'linewidth',3);
m_contour(xvc,yvc,tF2,[4 4],'-r','linewidth',3);
m_text(-18,78,'b. ','fontsize',30,'backgroundcolor',[1 1 1]);
title('2015','fontsize',30);
m_text(-5,68,'Winter 4°C','color',color1,'fontsize',20,'rotation',90,'backgroundcolor',[.9 .9 .9]);
m_text(-12,72,'Summer 4°C','color','r','fontsize',20,'rotation',90,'backgroundcolor',[.9 .9 .9]);

legend([h3 h4],{'Particules on March 1^s^t','Particules on August 31^s^t'},'Location','southeast','fontsize',20,'color',[1 1 1]);


hc = colorbar;
set(hc,'location','eastoutside','position',[0.92 0.19 0.008 0.65],'fontsize',20,'Ticks',[0:.002:.01],'TickLabels',[0:.2:1]);
ylabel(hc,'PIC x100 (\mumol/L)','fontsize',20)

% do uncomment for printing figure %
%export_fig ../FIGURES/FIG4_low.png -append -nocrop -png -r150 -transparent
%export_fig ../FIGURES/FIG4.png -append -nocrop -png -r300 -transparent
%export_fig ../FIGURES/FIG4.pdf -append -nocrop -eps -r300 -transparent -painters