clear all
close all
clc
%clf

% EXTRACT BATHYMETRY - you can implement your own here %
[ELEV,LON_ELEV,LAT_ELEV]=m_etopo2([10 100 68 80]);
ELEV(ELEV<-4000)=-4000;
[xxi,yyi]=meshgrid([-20:.1:100],[60:.1:80]);
elev=interp2(LON_ELEV,LAT_ELEV,ELEV,xxi,yyi);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

hFig=figure;
set(gcf,'PaperPositionMode','auto')
set(hFig, 'Position', [0 0 1600 700])

ax0=axes('position',[.45 .07 .5 .9]);
m_proj('Equidistant cylindrical','lon',[10 70],'lat',[68 80]);
hold on
[CS,CH]=m_contourf(xxi,yyi,elev,[-4000:1000:-1000 -500 -300 -200 -100 0],'edgecolor','none');
m_grid('linewi',2,'tickdir','out','fontsize',20,'XTick',[-20:20:70]);
m_gshhs_i('patch',[.6 .7 .6],'edgecolor','none');

% set up gray colorbar %
cmap=(gray(12));
cmap=colormap(cmap(4:12,:));
caxis([-900 0]);
%%%%%%%%%%%%%%%%%%%%%%%%

m_line(33,70,'marker','+','markersize',40,'markerfacecolor','k','color','k','linewidth',15)
m_text(34,69.5,'33°E-70°N','fontsize',20)

ax0b=axes('position',[.83 .8 .18 .18]);
m_proj('Equidistant cylindrical','lon',[-20 70],'lat',[60 80]);
m_grid('linewi',2,'tickdir','out','fontsize',20,'XTick',[-20:20:70],'XTickLabel',[],'YTickLabel',[]);
m_coast('patch',[.6 .7 .6],'edgecolor','none');
m_line([10 70 70 10 10],[68 68 80 80 68],'color','r','linewidth',3);

load ../DATA/LE_EXPANSION_95th_175km.mat
load ../DATA/DISTANCES_all.mat d_pic


ax1=axes('Position',[.08 .75 .3 .15]);
plot([1998:2016],d_pic,'.-k','markersize',15,'linewidth',3);

% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData([1998:2016],d_pic);
% Set up fittype and options.
ft = fittype( 'poly1' );
% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );
p=coeffvalues(fitresult);hold on
plot([1998:2016],p(1)*[1998:2016]+p(2),'color',[.8 .2 .2],'linewidth',5);
text(2010,100,['+ ',num2str(round(p(1)*19)),'  km'],'fontsize',20,'color',[.8 .2 .2]);
xlim([1998 2016]);
text(1998.5,900,'a. Ocean Color PIC','fontsize',20,'fontweight','bold')
ylabel('Distance (km)');
set(gca,'fontsize',18,'XTickLabel',[]);
ylim([0 1000]);



ax2=axes('Position',[.08 .55 .3 .15]);
plot([1998:2016],d_exp1,'.-k','markersize',15,'linewidth',3);

% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData([1998:2016],d_exp1);
% Set up fittype and options.
ft = fittype( 'poly1' );
% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );
p=coeffvalues(fitresult);hold on
plot([1998:2016],p(1)*[1998:2016]+p(2),'-','linewidth',5,'color',[.2 .7 .2]);
text(2010,100,['+ ',num2str(round(p(1)*19)),' km'],'fontsize',20,'color',[.2 .7 .2])
xlim([1998 2016]);
text(1998.5,900,'b. EXP1 : Influence of temperature + currents','fontsize',20,'fontweight','bold')
ylabel('Distance (km)');
set(gca,'fontsize',18,'XTickLabel',[]);
ylim([0 1000]);

ax3=axes('Position',[.08 .35 .3 .15]);
plot([1998:2016],d_exp2,'.-k','markersize',15,'linewidth',3);
% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData([1998:2016],d_exp2);
% Set up fittype and options.
ft = fittype( 'poly1' );
% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );
p=coeffvalues(fitresult);hold on
plot([1998:2016],p(1)*[1998:2016]+p(2),'-','linewidth',5,'color',[.8 .2 .8]);
text(2010,100,['+ ',num2str(round(p(1)*19)),' km (~ 56%)'],'fontsize',20,'color',[.8 .2 .8]);
xlim([1998 2016]);
text(1998.5,900,'c. EXP2 : Influence of currents','fontsize',20,'fontweight','bold');
text(1998.5,700,'(constant temperature)','fontsize',20);
ylabel('Distance (km)');
set(gca,'fontsize',18,'XTickLabel',[]);
ylim([0 1000]);


ax4=axes('Position',[.08 .15 .3 .15]);

h1=plot([1998:2016],d_exp3,'.-k','markersize',15,'linewidth',3);
% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData([1998:2016],d_exp3);
% Set up fittype and options.
ft = fittype( 'poly1' );
% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );
p=coeffvalues(fitresult);hold on
plot([1998:2016],p(1)*[1998:2016]+p(2),'linewidth',5,'color',[.2 .2 .8]);
text(2010,100,['+ ',num2str(round(p(1)*19)),' km (~ 44%)'],'fontsize',20,'color',[.2 .2 .8]);
xlim([1998 2016]);
text(1998.5,900,'d. EXP3 : Influence of temperature','fontsize',20,'fontweight','bold')
text(1998.5,700,'(constant currents)','fontsize',20)
ylabel('Distance (km)');
set(gca,'fontsize',18);
ylim([0 1000]);

% do uncomment for printing figure %
%export_fig ../FIGURES/FIG5.png -append -nocrop -png -r300 -transparent
%export_fig ../FIGURES/FIG5_low.png -append -nocrop -png -r150 -transparent
%export_fig ../FIGURES/FIG5.pdf -append -nocrop -pdf -r300 -transparent -painters