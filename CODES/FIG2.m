close all
clear all
clc

load ../DATA/LATLON.mat
load ../DATA/TIME.mat
load ../DATA/EOF.mat
load ../DATA/LEADINGEDGE.mat
leading_edge_anom=leading_edge-mean(leading_edge);

hFig=figure;
set(gcf,'PaperPositionMode','auto')
set(hFig, 'Position', [0 0 1200 1000])


ax1=axes('position',[.04 .52 .9 .46]);
m_proj('Equidistant cylindrical','lon',[-20 70],'lat',[60 80]);
m_contourf(LON,LAT,squeeze(eof_maps(:,:,1))+squeeze(eof_maps(:,:,2)),[-.015:.001:.015],'linestyle','none');
m_grid('linewi',2,'tickdir','out','fontsize',24,'XTick',[-20:20:70]);
m_gshhs_i('patch',[.6 .6 .6],'edgecolor','none');
cmocean('balance')
m_text(-10,77,'a. EOF-1 + EOF-2','fontsize',30,'fontweight','bold');
caxis([-.015 .015]);

hc = colorbar;
set(hc,'location','eastoutside','position',[0.78 0.52 0.02 0.46],'fontsize',20,'Ticks',[-.015:.005:.015],'TickLabels',[-15:5:15],'fontsize',20);
ylabel(hc,'Elevation (mm)','fontsize',24)


mooring=1;

ax2=axes('position',[.04 .04 .9 .4]);
plot(time,pc(1,:)+pc(2,:))
set(gca,'color','none','fontsize',24);
xlim([time(1) time(end)]);
hold on
anomaly(time,pc(1,:)+pc(2,:));
xlim([time(1) time(end)]);
text(datenum(1994,1,1),16,'b.','fontsize',30,'fontweight','bold');
ylim([-22 22]);
ax6.XTick=[time(1:24:end)];
datetick('x','yyyy','keeplimits','keepticks');
ylim([-26 26]);
box on

ax3=axes('position',[.04 .04 .9 .4]);
ax3.YAxisLocation='right';
ax3.YColor='r';
hold on
years_leading_edge(19)=2016.9;
h1=plot(datenum(years_leading_edge(leading_edge_anom>=0),1,1),leading_edge_anom(leading_edge_anom>=0),'ro','markersize',20,'Markerfacecolor','r')
set(gca,'color','none');
h2=plot(datenum(years_leading_edge(leading_edge_anom<0),1,1),leading_edge_anom(leading_edge_anom<0),'bo','markersize',20,'Markerfacecolor','b')
set(gca,'color','none','fontsize',20);
datetick('x','yyyy','keeplimits');
xlim([time(1) time(end)]);
ylim([-3 3]);
ax3.XTick=[];
ax3.YTick=[-3.5 -2.5 -1.5 -0.5 .5 1.5 2.5 3.5];
ax3.YTickLabel=[70 71 72 73 74 75 76 77];
ylabel('Leading Edge (N)','fontsize',20);

% do uncomment for printing figure %
%export_fig ../FIGURES/FIG2.png -append -nocrop -png -r300 -transparent
%export_fig ../FIGURES/FIG2_low.png -append -nocrop -png -r150 -transparent
%export_fig ../FIGURES/FIG2.pdf -append -nocrop -pdf -r300 -transparent -painters
